#!/bin/bash
yum update -y
yum install -y java-1.8.0-openjdk.x86_64
yum erase -y java-1.7.0-openjdk
yum install -y git
#wget http://repos.fedorapeople.org/repos/dchen/apache-maven/epel-apache-maven.repo -O /etc/yum.repos.d/epel-apache-maven.repo
#sed -i s/\$releasever/6/g /etc/yum.repos.d/epel-apache-maven.repo
#yum install -y apache-maven
yum install -y tomcat7
/etc/init.d/tomcat7 start
mkdir -p DOME
cd DOME
rm -rf *
cp DOMEApiServicesV7.war /var/lib/tomcat7/webapps
echo "queue=tcp://$ActiveMQdns:61616" >> config.properties
cp config.properties /var/lib/tomcat7/webapps/DOMEApiServicesV7/WEB-INF/classes/config/config.properties
/etc/init.d/tomcat7 restart
