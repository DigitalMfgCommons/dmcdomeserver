#!/bin/bash
cd DOME
rm -rf *
git clone https://bitbucket.org/DigitalMfgCommons/dmcdomeserver.git
cd dmcdomeserver
#mvn package
cp DOMEApiServicesV7.war /var/lib/tomcat7/webapps
echo "queue=tcp://$ActiveMQdns:61616" >> config.properties
cp config.properties /var/lib/tomcat7/webapps/DOMEApiServicesV7/WEB-INF/classes/config/config.properties
/etc/init.d/tomcat7 restart
